#!/usr/bin/env node

const os = require('os');

const shell = require('shelljs');

const executor = require('./index')();

if (os.type() !== 'Windows_NT') {
  shell.chmod(755, executor);
}

const child = shell.exec(`${executor} config`, { async: true });

// 输入重定向
process.stdin.pipe(child.stdin);
