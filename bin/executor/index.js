const os = require('os');
const path = require('path');

module.exports = function getExecutor() {
  const os_type = os.type();
  let bin = null;

  switch (os_type) {
    case 'Linux':
      bin = 'ossutil64';
      break;
    case 'Darwin':
      bin = 'ossutilmac64';
      break;
    default:
      bin = 'ossutil64.exe';
  }

  return path.join(__dirname, bin);
};
