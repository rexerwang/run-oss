#!/usr/bin/env node

const os = require('os');
const path = require('path');
const fs = require('fs');

const program = require('commander');
const shell = require('shelljs');
const chalk = require('chalk');

const executor = require('./executor')();

const pkg = require('../package.json');

const defaultConfigFile = path.join(os.homedir(), '.runossconfig.json');

program
  .version(pkg.version)
  .option('-c, --config <value>', '配置文件（默认配置`~/.runossconfig.json`）')
  .option('-e, --env <value>', '构建环境')
  .option('-t, --target <value>', '构建目标')
  .option('-b, --branch <value>', '指定分支（默认配置中`branch`，次之环境名）')
  .option('--publish', '直接发布')
  .parse(process.argv);

// 配置文件
const config = readJSON(program.config || defaultConfigFile);

if (!config) {
  logger('配置缺失', 'error');
  process.exit(1);
}

if (!program.target || !program.env) {
  logger('参数错误\nFor help: --help', 'error');
  process.exit(1);
}

if (!config[program.target]) {
  logger(`构建目标不匹配：${program.target}`, 'error');
  process.exit(1);
}

// 目标配置
const targetConfig = config[program.target];

if (!targetConfig.env[program.env]) {
  logger('未预设该环境：', program.env, 'error');
  process.exit(1);
}

// 目标环境配置
const envConfig = targetConfig.env[program.env];

// 构建分支
const branch = program.branch || config.branch || program.env;

logger(chalk`
目标工程: {black.bgYellow ${program.target}}
目标目录: {black.bgYellow ${targetConfig.path}}
目标环境: {black.bgYellow ${program.env}}
目标分支: {black.bgYellow ${branch}}
Bucket: {black.bgYellow ${envConfig.bucket}}
`);

// 本地构建
if (!program.publish) {
  build(targetConfig.path, envConfig, branch);
} else {
  logger('跳过本地构建，直接推送OSS', 'info');
}

// 推送至OSS
publish(envConfig.bucket, program.target, path.join(targetConfig.path, 'dist'));

/**
 * 读取json
 * @param {String} file 路径
 *
 * @throws {Error}
 */
function readJSON(file) {
  return JSON.parse(fs.readFileSync(file));
}

/**
 * build
 */
function build(workspace, config, branch) {
  if (!shell.which('git')) {
    logger('Sorry, this script requires git', 'error');
    shell.exit(1);
  }

  const content = config.build_env.join(os.EOL);

  shell.cd(workspace);
  shell.echo(workspace);

  // 切换分支
  if (shell.exec(`git checkout ${branch}`).code !== 0) {
    shell.exit(1);
  }

  // 拉取最新代码
  if (shell.exec('git pull').code !== 0) {
    shell.exit(1);
  }

  // 环境变量配置
  fs.writeFileSync(path.join(workspace, '.env'), content, 'utf8');

  // 本地构建
  if (shell.exec('npm run build').code !== 0) {
    shell.echo('构建失败');
    shell.exit(1);
  }
}

/**
 * 推送
 */
function publish(bucket, dir, dist) {
  const rm_cmd = `${executor} rm oss://${bucket}/${dir}/ -r --force`;
  const cp_cmd = `${executor} cp -r ${dist} oss://${bucket}/${dir}/`;

  if (shell.exec(rm_cmd).code !== 0) {
    logger('OSS目录删除失败', 'error');
    shell.exit(1);
  }

  if (shell.exec(cp_cmd).code !== 0) {
    logger('推送失败', 'error');
    shell.exit(1);
  } else {
    logger(chalk`
部署已完成：{black.bgYellow oss://${bucket}/${dir}/}
    `);
  }
}

function logger(msg, type) {
  let log = msg;
  switch (type) {
    case 'log':
      log = chalk`{black.bgYellow ${msg}}`;
      break;
    case 'info':
      log = chalk`{bgCyan INFO: ${msg}}`;
      break;
    case 'error':
      log = chalk`{bgRed ERROR: ${msg}}`;
      break;
  }
  console.log(log);
}
