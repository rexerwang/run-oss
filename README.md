# run-oss CLI

## 本地安装

1. 执行命令`npm link`
2. 填写 OSS 账号

## .runossconfig.json 配置

> 将`.runossconfig.json`复制到用户根目录

```json
{
  "targetProjectName": {
    "path": "项目路径",
    "env": {
      "dev": {
        "bucket": "dev环境的bucket",
        "branch": "dev环境对应的分支",
        "build_env": ["生成.env文件", "数组对应多行"]
      },
      "otherEnv": {}
    }
  },
  "otherProject": {}
}
```

## 使用

执行命令: `run-oss --help` 可查看帮助

```bash

# 发布dev环境项目：打包+推送至OSS
run-oss -t targetProjectName -e dev

# 发布dev环境项目：不重新打包，直接推送至OSS
run-oss -t targetProjectName -e dev --publish

# 发布dev环境项目：自定义配置文件
run-oss -t targetProjectName -e dev --config ./yourconfigfile.json

# 发布dev环境项目：自定义分支
run-oss -t targetProjectName -e dev --branch feature/xxx

```
